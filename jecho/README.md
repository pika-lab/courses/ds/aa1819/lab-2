1. Gradlefy the provided JEcho app

0. Dockerify it

0. Instantiate two containers, `jecho-d` and `jecho-g`

0. Try interacting with `jecho-d`, both from the host and from `jecho-g`

0. Test the instances ability to communicate

0. Describe your tests here, by editing this file
