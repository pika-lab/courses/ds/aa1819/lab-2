import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;

public class JEcho {
    public static final int PORT = 8080;
    private static final int BUFF_SIZE = 1024;

    private static boolean hasArg(String[] args, String arg) {
        return Arrays.stream(args).anyMatch(a -> a.equalsIgnoreCase(arg));
    }

    public static void main(String[] args) {
        try {
            if (hasArg(args, "--gate") || hasArg(args, "-g")) {
                gate(args[1], PORT);
            } else if (hasArg(args, "--daemon") || hasArg(args, "-d")) {
                daemon(PORT);
            } else {
                System.out.println("Syntax:");
                System.out.println("\tjava -cp <CLASSPATH> JEcho MODE [HOSTNAME]");
                System.out.println("where");
                System.out.println("\tMODE can be (--daemon|-d) or (--gate|-g)");
                System.out.println("\tHOSTNAME is only required for --gate mode");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int receive(Socket socket, byte[] buffer) throws IOException {
        return read(socket.getInputStream(), buffer);
    }

    private static int read(InputStream stream, byte[] buffer) throws IOException {
        return stream.read(buffer);
    }

    private static void send(Socket socket, byte[] buffer, int len) throws IOException {
        socket.getOutputStream().write(buffer, 0, len);
    }

    private static void daemon(int port) throws IOException {
        final ServerSocket server = new ServerSocket(port);

        while (true) {
            System.out.printf("Waiting for incoming connections on port %d\n", port);
            final Socket ephemeral = server.accept();
            System.out.printf("Data from '%s':\n", ephemeral.getRemoteSocketAddress());
            final byte[] buffer = new byte[BUFF_SIZE];
            for (int read = receive(ephemeral, buffer); read >= 0; read = receive(ephemeral, buffer)) {
                send(ephemeral, buffer, read);
                System.out.write(buffer, 0, read);
            }
            System.out.println();
            ephemeral.close();
        }
    }

    private static void gate(String host, int port) throws IOException, InterruptedException {
        final Socket client = new Socket(host, port);

        final Thread receiver = createReceiverThread(client);

        receiver.start();

        final byte[] buffer = new byte[BUFF_SIZE];
        for (int read = read(System.in, buffer); read >= 0; read = read(System.in, buffer)) {
            send(client, buffer, read);
        }

        client.getOutputStream().close();

        receiver.join();

        client.close();
    }

    private static Thread createReceiverThread(Socket socket) {
        return new Thread(() -> {
            try {
                System.out.printf("Echoing from %s:\n", socket.getRemoteSocketAddress());
                final byte[] buffer = new byte[BUFF_SIZE];
                for (int read = receive(socket, buffer); read >= 0; read = receive(socket, buffer)) {
                    System.out.write(buffer, 0, read);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                System.out.printf("Done with %s\n", socket.getRemoteSocketAddress());
            }
        });
    }
}
